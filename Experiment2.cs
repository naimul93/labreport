﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReport
{
    public class Experiment2
    {
        public static void Exp2()
        {
            int num, sum, temp, rem;
            int stno, enno;
            Console.WriteLine("Find Armstrong number of a given range:  \n");
            Console.WriteLine();
            Console.WriteLine("Enter starting number of the given range: ");
            stno = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter ending number of the given range:  ");
            enno = int.Parse(Console.ReadLine());

            Console.WriteLine("Armstrong numbers in given range are: ");


            for(num=stno;num<=enno;num++)
            {
                temp = num;
                sum = 0;
                while(temp!=0)
                {
                    rem = temp % 10;
                    temp = temp / 10;
                    sum = sum + (rem*rem*rem);
                }

                if(sum==num)
                {
                    Console.Write("{0}  ", num);
                }
            }

            Console.WriteLine();
        }
    }
}
